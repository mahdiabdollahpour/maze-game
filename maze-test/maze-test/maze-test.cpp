// mymaze-dfs.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <windows.h>
#include <conio.h>
#include <fstream>
#include <string>
#include <sstream>
using namespace std;
int width=23;
int hight=23;
const int e=1;
const int w=2;
const int sw=3;//seen wall
const int se=4;//seen char
const char slider='$';
enum color
{
	blue=9,
	black=0,
	green=10,
	white=7,
	red=12,
	lightblue=11,
	yellow=14,
};
enum direction
{
	up=72,
	down=80,
	_left=75,
	_right=77,
	shoot=32,
	pause='p',
	menu='q',
	save='s',

};
class load
{
public:
	int value;
	int indx;
	load()
	{
		value=0;
		indx=0;

	}

};
inline void gotoXY(int x, int y) {

	//Initialize the coordinates

	COORD coord = {x, y};

	//Set the position

	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);

	return;

}
inline void getCursorXY(int &x, int&y) {

	CONSOLE_SCREEN_BUFFER_INFO csbi;

	if(GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi)) {

		x = csbi.dwCursorPosition.X;

		y = csbi.dwCursorPosition.Y;
	}

}


inline void setcolor(color back=black,color text=white)
{
	HANDLE hstdout = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hstdout,  16 *back+ text);
}
void giveit(string str,load* load)
{
	int a=0;
	int i=load->indx;
	while (str[i]!=slider)
	{

		a=a*10+(str[i]-'0');

		i++;
	}

	load->indx=i+1;
	load->value=a;

}
bool oktodig(int** arr,int x,int y)
{
	if(x>=width || y>=hight || x<0 || y<0)
	{
		return false;

	}
	else if (arr[x][y]==e)
	{
		return false;

	}
	else 
	{
		return true;
	}
}
bool oktodig2(int** arr,int x,int y)
{
	if((x==0 && y==0) && arr[x][y]==w)
	{

		return true;
	}
	else
	{

		return (oktodig(arr,x+2,y)  || oktodig(arr,x-2,y) || oktodig(arr,x,y+2) || oktodig(arr,x,y-2)) && oktodig(arr,x,y);
	}

}
void saveit(int** arr,int x,int y,int moves,time_t time)
{
	char sdate[9];
	char stime[9];
	ofstream log;
	ofstream list;
	_strdate_s(sdate);
	_strtime_s(stime);
	string filename = "maze-";
	filename.append(stime);
	filename.append("_");
	filename.append(sdate);
	filename.append(".txt");
	for(int i = 0; i<filename.length(); ++i){
		if (filename[i] == '/' || filename[i] == ':')
			filename[i] = '-';
	}
	log.open(filename.c_str());
	if (log.fail())
	{
		perror(filename.c_str());
	}

	list.open("list.txt",ios::app);

	list<<filename;
	log<<hight;
	log<<slider;
	log<<width;
	log<<slider;
	log<<x;
	log<<slider;
	log<<y;
	log<<slider;
	log<<moves;
	log<<slider;
	log<<time;
	log<<slider;

	for(int i=0;i<width;i++)
	{
		for(int j=0;j<hight;j++)
		{
			log<<arr[i][j];
			log<<slider;
		}
	}

	log.close();
	list.close();




}
bool dig(int** arr,int x,int y)
{
	int dirs[4]={0,1,2,3};//down up left right
	//		   1
	//		2     3
	//         0
	int dir = dirs[rand()%4];

	/*if( !(oktodig(arr,x+2,y)  && oktodig(arr,x-2,y) && oktodig(arr,x,y+2) && oktodig(arr,x,y-2)))

	{
	return;
	}*/

	for(int i=0;i<4;i++)
	{
		if(dir==0)
		{
			if(oktodig2(arr,x,y-2))
			{
				arr[x][y]=e;
				arr[x][y-2]=e;
				arr[x][y-1]=e;
				if(	dig(arr,x,y-2))
				{
					return true;
				}
			}
		}
		else if(dir==1)//up
		{
			if(oktodig2(arr,x,y+2))
			{
				arr[x][y]=e;
				arr[x][y+2]=e;
				arr[x][y+1]=e;
				if(dig(arr,x,y+2))
				{
					return true;
				}
			}
		}

		else if(dir==2)//left
		{
			if(oktodig2(arr,x-2,y))
			{
				arr[x][y]=e;
				arr[x-2][y]=e;
				arr[x-1][y]=e;
				if(	dig(arr,x-2,y))
				{
					return true;
				}
			}
		}
		else if(dir==3)//left
		{
			if(oktodig2(arr,x+2,y))
			{
				arr[x][y]=e;
				arr[x+2][y]=e;
				arr[x+1][y]=e;
				if(dig(arr,x+2,y))
				{
					return true;
				}
			}
		}

		dir=(dir+1)%4;

	}

	return false;


}

int _tmain(int argc, _TCHAR* argv[])
{
	srand(time(NULL));
	cout<<"Do you want to start new game or you want to load a game?"<<endl;
	cout<<"1.New game?"<<endl<<"2.Load game"<<endl;
	bool newgame=false,loadgame=false;
	char c;
	do
	{
		c=_getch();
	}
	while(!(c=='1' || c=='2'));
	switch (c)
	{
	case '1':
		newgame=true;
		break;
	case '2':
		loadgame=true;

	default:
		break;
	}

	int x,y;//the place of mouse
	int moves;
	int** arr=NULL;
	time_t stime;
	if(newgame)
	{
		cout<<"enter the puzzle hight"<<endl;
		cin>>width;
		cout<<"enter the puzzle width"<<endl;
		cin>>hight;

		//char arr[width][hight];
		arr=new int*[width];// index aval
		for(int i=0;i<width;i++)
		{
			arr[i]=new int[hight];
		}


		for(int i=0;i<width;i++)
		{
			for(int j=0;j<hight;j++)
			{

				arr[i][j]=w;

			}
		}

		dig(arr,2*((width-1)/2),2*((hight-1)/2));
		//dig(arr,0,0);

		int sx=0,sy=0;

		arr[0][0]=se;
		x=0;
		y=0;
		moves =0;

		stime=time(NULL);
	}

	else if(c='2')//load gameeeeeeeeeeeeeeeeeeeee
	{

		fstream list;
		list.open("list.txt");
		string myArray;
		if(list.is_open())
		{

			for(int i = 0; !list.eof() && i<10; ++i)
			{
				list >> myArray;
			}
		}
		system("cls");
		cout<<"you have already saved below files:"<<endl;
		for(int i=0;i< 1000 &&  myArray[i]!='\0';i++)
		{
			cout<<myArray[i];
			if((i+1)%26==0)
			{
				cout<<endl;
			}
		}

		string loadit;
		cout<<"Enter your game name to load it."<<endl;
		cin>>loadit;
		ifstream loadfile;
		loadfile.open(loadit.c_str());
		if(loadfile.is_open())
		{
			cout<<loadit<<" was loaded"<<endl;

			stringstream strStream;
			strStream << loadfile.rdbuf();//read the file
			string str = strStream.str();//str holds the content of the file
			load L;
			giveit(str,&L);
			hight=(int)L.value;
			giveit(str,&L);
			width=(int)L.value;
			giveit(str,&L);
			x=(int)L.value;
			giveit(str,&L);
			y=(int)L.value;
			giveit(str,&L);
			moves=(int)L.value;
			giveit(str,&L);
			stime=time(NULL);
			stime+=(time_t)L.value;
			arr=new int*[width];// index aval
			for(int i=0;i<width;i++)
			{
				arr[i]=new int[hight];
			}


			for(int i=0;i<width;i++)
			{
				for(int j=0;j<hight;j++)
				{
					giveit(str,&L);
					arr[i][j]=(int)L.value;
					

				}
			}

		}
	}
	int ex=2*((width-1)/2);
	int ey=2*((hight-1)/2);



	while(!(x==2*((width-1)/2) && y==2*((hight-1)/2)))
	{
		system("cls");

		
		for(int j=0;j<hight+2;j++)
		{
			cout<<'_';
		}
		cout<<endl;
		for(int i=0;i<width;i++)
		{
			cout<<'|';

			for(int j=0;j<hight;j++)
			{
				if(arr[i][j]==e)
				{
					setcolor(blue);
				}
				if(arr[i][j]==w)
				{
					setcolor(blue);
				}
				if(arr[i][j]==se)
				{
					setcolor(green);
				}
				if(arr[i][j]==sw)
				{
					setcolor(red);
				}
				if(i==x && j==y)
				{
					setcolor(yellow);
				}
				if(i==ex && j==ey)
				{
					setcolor();
				}
			//	cout<<arr[i][j];
				cout<<' ';
				setcolor();
			}
			cout<<'|'<<endl;
		}
		for(int j=0;j<hight+2;j++)
		{
			cout<<'#';
		}
		cout<<endl;

		switch ((direction)_getch())
		{

		case _left:


			if( y>0 && (arr[x][y-1]==e || arr[x][y-1]==se ))
			{
				arr[x][y-1]=se;
				y--;
				moves++;

			}
			else if(y>0 && (arr[x][y-1]== w || arr[x][y-1]==sw ))
			{
				arr[x][y-1]=sw;
				moves++;
			}
			break;
		case _right:
			if(y<hight-1 && (arr[x][y+1]==e || arr[x][y+1]==se) )
			{

				arr[x][y+1]=se;
				y++;
				moves++;
			}
			else if(y<hight-1 && (arr[x][y+1]== w  || arr[x][y+1]==sw )  )
			{
				arr[x][y+1]=sw;
				moves++;
			}

			break;
		case up:
			if(x>0 && (arr[x-1][y]==e || arr[x-1][y]==se)  )
			{

				arr[x-1][y]=se;
				x--;
				moves++;
			}
			else if(x>0 &&( arr[x-1][y]== w  || arr[x-1][y]==sw ))
			{
				arr[x-1][y]=sw;
				moves++;
			}





			break;
		case down:
			if(x<width-1 &&(arr[x+1][y]==e || arr[x+1][y]==se))
			{
				arr[x+1][y]=se;
				x++;
				moves++;

			}
			else if(x<width-1 && (arr[x+1][y]== w || arr[x+1][y]==sw))
			{
				arr[x+1][y]=sw;
				moves++;
			}



			break;
		case save:
			
				time_t now=time(NULL);
				time_t save=now-stime;
				saveit(arr,x,y,moves,save);
			
			break;
	//	default:
			//break;
		}



	}







	for(int i=0;i<width;i++)
	{
		delete[] arr[i];
	}

	delete[] arr;


	time_t t;
	t=time(NULL);
	cout<<"you finished game with "<<moves<<" moves"<<endl;
	cout<<"you finished game in "<<t-stime<<" seconds"<<endl;


	system("pause");

	return 0;
}

